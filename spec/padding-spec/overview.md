<a id="padding-spec.txt-1"></a>

# Overview

Tor supports two classes of cover traffic: connection-level padding, and
circuit-level padding.

Connection-level padding uses the PADDING cell command for cover
traffic, where as circuit-level padding uses the DROP relay
command. PADDING cells are single-hop only and can be differentiated from
normal traffic by Tor relays ("internal" observers), but not by entities
monitoring Tor OR connections ("external" observers).

DROP relay messages are multi-hop, and is not visible to intermediate Tor
relays, because the relay command field is covered by circuit layer
encryption. Moreover, Tor's 'recognized' field allows DROP messages
to be sent to any intermediate node in a circuit (as per Section
6.1 of tor-spec.txt).

Tor uses both connection level and circuit level padding. Connection
level padding is described in section 2. Circuit level padding is
described in section 3.

The circuit-level padding system is completely orthogonal to the
connection-level padding. The connection-level padding system regards
circuit-level padding as normal data traffic, and hence the connection-level
padding system will not add any additional overhead while the circuit-level
padding system is actively padding.
