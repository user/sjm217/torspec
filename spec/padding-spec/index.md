# Tor Padding Specification

Mike Perry, George Kadianakis

Note: This is an attempt to specify Tor as currently implemented.  Future
versions of Tor will implement improved algorithms.

This document tries to cover how Tor chooses to use cover traffic to obscure
various traffic patterns from external and internal observers. Other
implementations MAY take other approaches, but implementors should be aware of
the anonymity and load-balancing implications of their choices.
