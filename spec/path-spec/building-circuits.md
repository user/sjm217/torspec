<a id="path-spec.txt-2"></a>

# Building circuits

Here we describe a number of rules for building circuits: under what
circumstances we do so, how we choose the paths for them, when we give
up on an in-progress circuits, and what we do when circuit
construction fails.

