<a id="dir-spec.txt-3.4.3"></a>

# Serving bandwidth list files { #serving-bwlist }

If an authority has used a bandwidth list file to generate a vote
document it SHOULD make it available at

`http://<hostname>/tor/status-vote/next/bandwidth.z`

at the start of each voting period.

It MUST NOT attempt to send its bandwidth list file in a HTTP POST to
other authorities and it SHOULD NOT make bandwidth list files from other
authorities available.

If an authority makes this file available, it MUST be the bandwidth file
used to create the vote document available at

`http://<hostname>/tor/status-vote/next/authority.z`

To avoid inconsistent reads, authorities SHOULD only read the bandwidth
file once per voting period. Further processing and serving SHOULD use a
cached copy.

The bandwidth list format is described in bandwidth-file-spec.txt.

The standard URLs for bandwidth list files first-appeared in
Tor 0.4.0.4-alpha.

<a id="dir-spec.txt-3.5"></a>
